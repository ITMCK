/*
  ITMCK
  Version 0.1
  Licensed under GNU GPL v3 or later version
*/

#define itmck_version "ITMCK v0.1"
#define itmck_version_code 0x7010
#define impulse_tracker_version_code 0x0204

#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FX(x) ((x)&0x1F)
#define FXC(x) (FX(x)+26)
#define C_pitch 261.6255653
#define tau (2.0*M_PI)
#define rand_double() (2.0 * ((double) rand() / (double)RAND_MAX - 0.5))
#define unequal3(x,y,z) ((x)!=(y) && (x)!=(z) && (y)!=(z))
#define unequal3lt(x,y,z) ((x)!=(y) && (x)<(z) && (y)<(z))
#define ITout stdout

typedef unsigned char byte;
typedef byte boolean;

typedef struct chan_info {
  short transpose;
  byte octave;
  short length;
  byte instrument;
  byte effectid; // 0=not set
  byte effectpar;
  byte volumeset; // 255=not set
  short quantize; // <0=deduct frames >0=fraction of time
  byte note_cuts; // 255=off, 254=cut, 253=fade, 250=continue
  byte ch_vol; // use for .IT header: 0-64
  byte ch_pan; // use for .IT header: 0-64, 32=center, 100=surround
  int row;
  int row_cut; // use for slurs/ties and auto portamento
  int row_tie; // use for ties
  int loopstart;
  int loopskip;
  int loopend;
  byte keytable; // 255=unuse
  byte customfx; // 255=unuse
  boolean auto_portamento; // set if next command should auto portamento
  byte prev_note; // use for tie and auto portamento
} chan_info;

typedef struct message_line {
  char*data;
  struct message_line*next;
} message_line;

typedef struct chan_row {
  byte note; // 200=unuse
  byte instrument; // 255=unuse
  byte effectid; // 255=unuse
  byte effectpar;
  byte volumeset; // 255=unuse
  byte continue_flag; // 0x01=effect 0x02=volume 0x04=zero next effect 0x80=used
} chan_row;

typedef struct chan_row_block {
  chan_row data[17]; // data[16]=global effects
  boolean used; // keep track of which row contains data
} chan_row_block;

typedef struct key_setting {
  byte note;
  byte instrument; // 255=unuse
  byte effectid; // 0=unuse
  byte effectpar;
  byte volumeset; // 255=unuse
  byte customfx; // 255=unuse
} key_setting;

typedef struct instrument_data {
  byte heading[11]; // address 0x0011 to 0x001B
  byte ifc; // cutoff 0-127; bit7=enable
  byte ifr; // resonance 0-127; bit7=enable
  byte keyboard[240];
  byte vol_env[82];
  byte pan_env[82];
  byte pitch_env[82]; // bit7 of flag enables filter envelope
  boolean used; // set if instrument is selected in MML
} instrument_data;

typedef struct sample_data {
  byte global_volume;
  byte flag;
  byte default_volume;
  byte convert;
  byte pan;
  int length;
  int loop_start;
  int loop_end;
  int c5speed;
  int sustain_start;
  int sustain_end;
  byte vibrato[4];
  byte make; // Mode to make data: 1=raw-file 2=synth 3=copy 4=IMPS-file
  byte link; // Depend on 'make'
  char*name; // Filename or depend on 'make'
  boolean used; // set when compiling instruments, before compiling samples
} sample_data;

typedef struct custom_fx {
  short transpose; // 255=omit note
  byte instrument; // 255=unuse
  byte effectid; // 0=unuse
  byte effectpar;
  byte volumeset; // 255=unuse
  byte continue_flag; // 0x01=effect 0x02=volume 0x04=zero next effect 0x80=continue pass note
  int delta;
  struct custom_fx*next;
} custom_fx;

byte it_flags=0x0E;
chan_info channels[16];
message_line*song_message;
message_line**next_song_message=&song_message;
int song_message_length=0;
short octave_dir=1;
char song_title[26];
byte gate_denom=8;
byte whole_note=48;
byte panning_separation=128;
byte init_tempo=120;
char scale_base='c';
char*macro_text[256][16];
byte g_vol=128;
byte g_mixer=48;
key_setting keytab[64][120];
custom_fx fxtab[64];
unsigned short tracker_version=itmck_version_code; // Cwt
sample_data samples[100];
instrument_data instruments[100];
byte max_sample=0;
byte max_instrument=0;
byte compatibility=0; // 0=Impulse Tracker
int numeric_register[27]; // register 0 ('`') is special
char*filename_prefix="";
byte export_sample=0;
chan_row_block*frames;
int max_frames=0;
boolean synth16bits=0;
int synth_len_mult=1;
boolean trace_input=0;
unsigned long out_offset=0x00000000UL;

//                         A ' B | C ' D ' E | F ' G '  (h)  (i)  (j)
short note_letter[10] = { -3, -1,  0,  2,  4,  5,  7,    0,   0,   0  };
int octave_tones=12;

//Predeclare
void read_file(FILE*fp);

inline void write8(int x) {
  putchar(x&0xFF);
}

inline void write16(int x) {
  putchar(x&0xFF);
  putchar((x>>8)&0xFF);
}

inline void write32(int x) {
  putchar(x&0xFF);
  putchar((x>>8)&0xFF);
  putchar((x>>16)&0xFF);
  putchar((x>>24)&0xFF);
}

int read_int(char**s) {
  char c;
  int r=0;
  int q=1;
  int b=10;
  if(**s==',') (*s)++;
  if(**s=='$' && (*s)[1]>='`' && (*s)[1]<='z') return numeric_register[(*s)[1]&0x1F];
  if(**s=='+') (*s)++;
  if(**s=='-') { q=-1; (*s)++; }
  if(**s=='$') { b=16; (*s)++; }
  while((**s>='0' && **s<='9') || (b==16 && **s>='A'&& **s<='F')) {
    c=*((*s)++);
    r=r*b+c-('0'+7*(c>='A'));
  }
  return q*r;
}

int read_note(char**s) {
  int x;
  if(**s=='n') {
    ++*s;
    return read_int(s);
  }
  if(**s<'a' || **s>'j') return -1;
  x=*((*s)++);
  x=note_letter[x-'a']+octave_tones*(x<scale_base);
  while(**s) {
    if(**s=='+') { x++; ++*s; }
    else if(**s=='-') { x--; ++*s; }
    else if(**s=='\'') { x+=octave_tones; ++*s; }
    else if(**s=='"') { x+=octave_tones*2; ++*s; }
    else break;
  }
  return x+octave_tones*read_int(s);
}

void skip_spaces(char**s) {
  if(!s || !*s || !**s) return;
  while(**s && **s<=' ') (*s)++;
}

void skip_equals_brace(char**s) {
  if(!s || !*s || !**s) return;
  while(**s && (**s<=' ' || **s=='=')) (*s)++;
  if(**s=='{') (*s)++;
  while(**s && **s<=' ') (*s)++;
}

void allocate_frames(int n) {
  int x;
  if(n<=max_frames) return;
  if(n<0 || !(frames=realloc(frames,sizeof(chan_row_block)*n))) {
    fprintf(stderr,"*FATAL* Out of frame memory");
    exit(1);
  }
  while(max_frames<n) {
    frames[max_frames].used=0;
    for(x=0;x<17;x++) {
      frames[max_frames].data[x].note=200;
      frames[max_frames].data[x].effectid=frames[max_frames].data[x].instrument=frames[max_frames].data[x].volumeset=255;
      frames[max_frames].data[x].continue_flag=0;
    }
    max_frames++;
  }
}

void send_channel(byte ch,byte note,byte instrument,byte effectid,byte effectpar,byte volumeset,byte continue_flag,int rows) {
  if(channels[ch].row<0) {
    rows+=channels[ch].row;
    channels[ch].row=0;
  }
  if(rows<0) return;
  if(instrument<=max_instrument) instruments[instrument].used=1;
  allocate_frames(channels[ch].row+1);
  frames[channels[ch].row].used=1;
  if(unequal3lt(frames[channels[ch].row].data[ch].note,note,200)) fprintf(stderr,"Conflicting notes on channel %d.\n",ch);
  if(note!=200) frames[channels[ch].row].data[ch].note=note;
  if(note<200) channels[ch].prev_note=note;
  if(unequal3(frames[channels[ch].row].data[ch].instrument,instrument,255)) fprintf(stderr,"Conflicting instruments on channel %d.\n",ch);
  if(instrument!=255) frames[channels[ch].row].data[ch].instrument=instrument;
  if(unequal3(frames[channels[ch].row].data[ch].effectid,effectid,255)) fprintf(stderr,"Conflicting effects on channel %d.\n",ch);
  if(effectid!=255) {
    frames[channels[ch].row].data[ch].effectid=effectid;
    frames[channels[ch].row].data[ch].effectpar=effectpar;
  }
  if(unequal3(frames[channels[ch].row].data[ch].volumeset,volumeset,255)) fprintf(stderr,"Conflicting volumes on channel %d.\n",ch);
  if(volumeset!=255) frames[channels[ch].row].data[ch].volumeset=volumeset;
  frames[channels[ch].row].data[ch].continue_flag|=continue_flag|0x80;
  channels[ch].row+=rows;
  if(continue_flag) send_channel(ch,200,255,255,0,255,0,0);
}

void send_global(int row,byte effectid,byte effectpar,byte volumeset,byte continue_flag,int rows) {
  if(row<0) {
    rows+=row;
    row=0;
  }
  if(rows<0) return;
  allocate_frames(row+1);
  frames[row].used=1;
  if(unequal3(frames[row].data[16].effectid,effectid,255)) fprintf(stderr,"Conflicting global effects on row %d.\n",row);
  if(effectid!=255) {
    frames[row].data[16].effectid=effectid;
    frames[row].data[16].effectpar=effectpar;
  }
  if(unequal3(frames[row].data[16].volumeset,volumeset,255)) fprintf(stderr,"Conflicting global volumes on row %d.\n",row);
  if(volumeset!=255) frames[row].data[16].volumeset=volumeset;
  frames[row].data[16].continue_flag|=continue_flag|0x80;
  if(continue_flag && rows) send_global(row+rows,255,0,255,0,0);
}

int at_backtick(byte ch,byte v,char c) {
  switch(c) {
    case '?': return v;
    case 'C': return channels[ch&15].row_cut%(*numeric_register?:1);
    case 'I': return channels[ch&15].instrument;
    case 'K': return channels[ch&15].transpose;
    case 'L': return channels[ch&15].length;
    case 'N': return channels[ch&15].prev_note;
    case 'O': return channels[ch&15].octave;
    case 'R': return channels[ch&15].row%(*numeric_register?:1);
    case 'T': return channels[ch&15].row_tie%(*numeric_register?:1);
    case 'i': return keytab[ch&63][v%120].instrument;
    case 'n': return keytab[ch&63][v%120].note;
    case 'u': return keytab[ch&63][v%120].customfx;
    case 'v': return keytab[ch&63][v%120].volumeset;
    default: fprintf(stderr,"Wrong @` command: %c\n",c);
  }
  return 0;
}

void hash_directive(char*s) {
  // s points to character after '#'
  char*t=s;
  while(*t>32) t++;
  if(*t) *t++=0;
  else t=0;
  skip_spaces(&t);
  if(!strcmp(s,"AMIGA-SLIDES")) {
    it_flags&=~0x08;
  } else if(!strcmp(s,"CHANNEL")) {
    int x;
    for(x=0;x<16;x++) {
      channels[x].ch_vol=read_int(&t);
      if(channels[x].ch_vol<0 || channels[x].ch_vol>64) fprintf(stderr,"Abnormal channel volume (%c).\n",x+'A');
      skip_spaces(&t);
      channels[x].ch_pan=read_int(&t);
      if((channels[x].ch_pan<0 || channels[x].ch_pan>64) && channels[x].ch_pan!=100)
        fprintf(stderr,"Abnormal channel panning (%c).\n",x+'A');
      skip_spaces(&t);
    }
  } else if(!strcmp(s,"COMPATIBILITY")) {
    compatibility=read_int(&t);
  } else if(!strcmp(s,"GATE-DENOM")) {
    gate_denom=read_int(&t);
    if(gate_denom<1) fprintf(stderr,"Abnormal gate denominator.\n");
  } else if(!strcmp(s,"INCLUDE")) {
    FILE*fp=fopen(t,"r");
    if(fp) {
      read_file(fp);
      fclose(fp);
    } else {
      fprintf(stderr,"File cannot be loaded: %s\n",t);
    }
  } else if(!strcmp(s,"LINEAR-SLIDES")) {
    it_flags|=0x08;
  } else if(!strcmp(s,"MINIMUM-VERSION")) {
    if(read_int(&t)>(itmck_version_code&0x0FFF)) {
      fprintf(stderr,"*FATAL* Input file requires later version of ITMCK.\n");
      exit(1);
    }
  } else if(!strcmp(s,"MIXER")) {
    g_mixer=read_int(&t);
    if(g_mixer==0 || g_mixer>128) fprintf(stderr,"Abnormal mix volume.\n");
  } else if(!strcmp(s,"MONO")) {
    it_flags&=~0x01;
  } else if(!strcmp(s,"OCTAVE-REV")) {
    octave_dir=read_int(&t)?-1:1;
  } else if(!strcmp(s,"OLD-EFFECTS")) {
    it_flags|=0x10;
  } else if(!strcmp(s,"PANNING-SEPARATION")) {
    panning_separation=read_int(&t);
    if(panning_separation<0 || panning_separation>128) fprintf(stderr,"Abnormal panning separation.\n");
  } else if(!strcmp(s,"PORTAMENTO-SHARE")) {
    it_flags|=0x20;
  } else if(!strcmp(s,"SCALE")) {
    int x=0;
    int b=0;
    char*y=t;
    while(*t++) {
      if(*t=='|') b=x;
      x+=(*t>' ');
    }
    x=-b;
    while(*y++) {
      if(*y>='a' && *y<='j') note_letter[*y-'a']=x;
      x+=(*y>' ' && *y!='|');
    }
  } else if(!strcmp(s,"SCALE-BASE")) {
    scale_base=*t;
    if(*t<'a' || *t>'j') fprintf(stderr,"Abnormal scale base (%s).\n",t);
  } else if(!strcmp(s,"STEREO")) {
    it_flags|=0x01;
  } else if(!strcmp(s,"SYNTH-LENGTH")) {
    synth_len_mult=read_int(&t);
  } else if(!strcmp(s,"SYNTH16")) {
    synth16bits=1;
  } else if(!strcmp(s,"TEMPO")) {
    init_tempo=read_int(&t);
    if(init_tempo<32) fprintf(stderr,"Abnormal tempo.\n");
  } else if(!strcmp(s,"TITLE")) {
    strncpy(song_title,t,26);
  } else if(!strcmp(s,"TRACKER-VERSION")) {
    tracker_version=read_int(&t);
  } else if(!strcmp(s,"VOL0MIXOPT")) {
    it_flags|=0x02;
  } else if(!strcmp(s,"VOLUME")) {
    g_vol=read_int(&t);
    if(g_vol>128) fprintf(stderr,"Abnormal global volume.\n");
  } else if(!strcmp(s,"WHOLE-NOTE")) {
    whole_note=read_int(&t);
    if(!whole_note) fprintf(stderr,"Abnormal whole note time.\n");
  } else {
    fprintf(stderr,"Unknown command: #%s\n",s);
  }
}

void read_sample_def(byte n, char*s) {
  char*t;
  int x,y;
  while(skip_spaces(&s),*s) {
    switch(*s++) {

      case '@': // Copy sample
        if(*s=='s') s++;
        x=read_int(&s);
        if(x>=n || !x) {
          fprintf(stderr,"Cannot copy sample %d to %d.\n",x,n);
        } else if(samples[n].make || samples[x].make==3) {
          fprintf(stderr,"Cannot copy a copy of a sample or change an existing sample into a copy.\n",x,n);
        } else {
          samples[n].make=3;
          samples[n].link=x;
          samples[n].global_volume=samples[x].global_volume;
          samples[n].flag=samples[x].flag;
          samples[n].default_volume=samples[x].default_volume;
          samples[n].convert=samples[x].convert;
          samples[n].pan=samples[x].pan;
          samples[n].loop_start=samples[x].loop_start;
          samples[n].loop_end=samples[x].loop_end;
          samples[n].c5speed=samples[x].c5speed;
          samples[n].sustain_start=samples[x].sustain_start;
          samples[n].sustain_end=samples[x].sustain_end;
        }
        break;

      case 'F': // File(IMPS)
        if(!samples[n].make) samples[n].make=4;
        if(samples[n].make!=4) fprintf(stderr,"Conflicting sample loading modes (%d).\n",n);
        if(*s++!='=') fprintf(stderr,"Equal sign expected before filename.\n",n);
        t=s;
        while(*s++>' ');
        *s++=0;
        samples[n].name=strdup(t);
        //todo
        break;

      case 'G': // Global volume
        samples[n].global_volume=read_int(&s);
        if(samples[n].global_volume>64) fprintf(stderr,"Abnormal sample global volume.\n");
        break;

      case 'L': // Loop
        samples[n].flag|=0x10;
        x=read_int(&s); y=read_int(&s);
        if(x<y) {
          samples[n].flag|=0x40;
          samples[n].loop_start=y;
          samples[n].loop_end=x;
        } else {
          samples[n].loop_start=x;
          samples[n].loop_end=y;
        }
        break;

      case 'S': // Sustain
        samples[n].flag|=0x20;
        x=read_int(&s); y=read_int(&s);
        if(x<y) {
          samples[n].flag|=0x80;
          samples[n].sustain_start=y;
          samples[n].sustain_end=x;
        } else {
          samples[n].sustain_start=x;
          samples[n].sustain_end=y;
        }
        break;

      case 'a': // Additive synthesizer
      case 'b': // Manual synthesizer
        if(!samples[n].make) samples[n].make=2;
        if(samples[n].make!=2) fprintf(stderr,"Conflicting sample loading modes (%d).\n",n);
        for(t=s;*t && *t!=']';t++);
        *t=0;
        samples[n].name=strdup(s-1);
        s=t+1;
        break;

      case 'd': // Default volume
        samples[n].default_volume=read_int(&s);
        if(samples[n].default_volume>64) fprintf(stderr,"Abnormal sample default volume.\n");
        break;

      case 'f': // File(raw)
        if(!samples[n].make) samples[n].make=1;
        if(samples[n].make!=1) fprintf(stderr,"Conflicting sample loading modes (%d).\n",n);
        if(*s++!='=') fprintf(stderr,"Equal sign expected before filename.\n",n);
        t=s;
        while(*s++>' ');
        *s++=0;
        samples[n].name=strdup(t);
        break;

      case 'r': // C5 speed
        if(!(samples[n].c5speed=read_int(&s))) fprintf(stderr,"Abnormal C5speed.\n");
        break;

      case 'v': // Vibrato
        x=*s++;
        if(x=='U') samples[n].vibrato[3]=0;
        if(x=='N') samples[n].vibrato[3]=1;
        if(x=='L') samples[n].vibrato[3]=2;
        if(x=='R') samples[n].vibrato[3]=3;
        else fprintf(stderr,"Invalid vibrato waveform: %c.\n",x);
        samples[n].vibrato[2]=read_int(&s);
        samples[n].vibrato[1]=read_int(&s);
        samples[n].vibrato[0]=read_int(&s);
        if(samples[n].vibrato[2]>64) fprintf(stderr,"Abnormal sample vibrato rate.\n");
        if(samples[n].vibrato[1]>64) fprintf(stderr,"Abnormal sample vibrato depth.\n");
        if(samples[n].vibrato[0]>64) fprintf(stderr,"Abnormal sample vibrato speed.\n");
        break;

      case 'y': // Pan
        samples[n].pan=read_int(&s)|128;
        break;

      case '}': // End of sample definition
        skip_spaces(&s);
        if(*s) fprintf(stderr,"Sample definition ends early.\n");
        return;

      default:
        fprintf(stderr,"Unknown command in sample definition: %c\n",s[-1]);
    }
  }
}

void read_envelope(byte*env,char**s) {
  int x;
  int skip=1;
  int ticks=0;
  byte*e2;
  if(1&*env) {
    fprintf(stderr,"This envelope is already set.\n");
    return;
  }
  *env|=1;
  while(skip_spaces(s),**s) {
    switch(*((*s)++)) {

      case '[': case '{': case '=': // Ignore
        break;

      case ']': case '}': // End
        if(!env[1]) fprintf(stderr,"This envelope has no nodes. (How does it smell?)\n");
        return;

      case '(': // Begin loop
        env[2]=env[1];
        break;

      case ')': // End loop
        env[3]=env[1]-1;
        *env|=2;
        break;

      case '<': // Begin sustain
        env[4]=env[1];
        break;

      case '>': // End sustain
        env[5]=env[1]-1;
        *env|=4;
        break;

      case '@': // Copy
        x=read_int(s);
        if(x<1 || x>max_instrument) {
          fprintf(stderr,"Attempting to copy envelope from nonexisting instrument.\n");
        } else {
          if(**s=='f' || **s=='i') e2=instruments[x].pitch_env;
          else if(**s=='v') e2=instruments[x].vol_env;
          else if(**s=='p') e2=instruments[x].pan_env;
          else return;
          *env|=*e2;
          memcpy(env+1,e2+1,61);
        }
        (*s)++;
        return;

      case 'a': // Advance
        ticks+=read_int(s);
        break;

      case 's': // Set skip
        skip=read_int(s);
        break;

      case '$': case '0' ... '9': case '-': case '+': // Values
        env[env[1]*3+6]=x=read_int(s);
        if(x<-32 || x>64) fprintf(stderr,"Abnormal envelope value.\n");
        env[env[1]*3+7]=ticks&0xFF;
        env[env[1]*3+8]=ticks>>8;
        env[1]++;
        ticks+=skip;
        break;

      default:
        fprintf(stderr,"Unknown command in envelope: %c\n", (*s)[-1]);
    }
    if(env[1]>25) {
      fprintf(stderr,"*FATAL* Too many nodes in envelope.\n");
      exit(1);
    }
  }
  if(!env[1]) fprintf(stderr,"This envelope has no nodes.\n");
}

void read_instrument_key_table(byte n,char**s) {
  int x,y,z1,z2,z3,z4;
  while(skip_spaces(s),**s) {
    switch(*((*s)++)) {

      case '[': case '{': case '=': // Ignore
        break;

      case ']': case '}': // End
        return;

      case '%': // Copy key assign along octaves
        x=read_int(s); y=read_int(s);
        if(y>0 && x>=y) {
          for(z1=z2=z3=0;z1<120;z1++,z2=(z2+1)%x,z3+=y*!z2) {
            instruments[n].keyboard[z1<<1]=instruments[n].keyboard[z2<<1]+z3;
            instruments[n].keyboard[1|(z1<<1)]=instruments[n].keyboard[1|(z2<<1)];
          }
        } else {
          fprintf(stderr,"Abnormal %% command in instruments key table.\n");
        }
        break;

      case 'a' ... 'j': case 'n': // Assign keys
        --*s;
        x=read_note(s);
        if(**s==':') {
          y=read_note(s);
          if(y==-1) fprintf(stderr,"Wrong note of range of assign key instrument table.\n");
          else if(y<x) fprintf(stderr,"Backward range of assign key instrument table.\n");
        } else {
          y=x;
        }
        if(**s=='=') ++*s;
        z2=read_note(s);
        if(z2>-1) {
          z1=0;
        } else {
          z1=1;
          z2=read_int(s);
        }
        if(**s=='@') {
          ++*s;
          if(**s=='s') ++*s;
          z3=read_int(s);
        } else {
          z3=0;
        }
        z4=(**s=='+'); *s+=z4;
        while(x<=y) {
          instruments[n].keyboard[x<<1]=x*z1+z2;
          if(z3) instruments[n].keyboard[1|(x<<1)]=z3;
          x++;
          z3+=z4;
        }
        break;

      default:
        fprintf(stderr,"Unknown command in instrument key table: %c\n", (*s)[-1]);
    }
  }
}

void read_instrument_def(byte n, char*s) {
  char*t;
  int x,y;
  while(skip_spaces(&s),*s) {
    switch(*s++) {

      case '@': // Set sample for all notes, or copy instrument
        if(*s=='s') {
          s++;
          x=read_int(&s);
          for(y=0;y<120;y++) instruments[n].keyboard[1|(y<<1)]=x;
        } else {
          x=read_int(&s);
          if(x<1 || x>99 || x==n) fprintf(stderr,"Instrument number out of range during copy.\n");
          else memcpy(instruments+n,instruments+x,sizeof(instrument_data));
        }
        break;

      case 'A': // Note action
        x=read_int(&s);
        instruments[n].heading[0]=x&3; // NNA
        instruments[n].heading[1]=(x/10)&3; // DCT
        instruments[n].heading[2]=(x/100)&3; // DCA
        if(x<0 || x%10>3 || (x/10)%10>3 || x/100>2) fprintf(stderr,"Abnormal NNA/DCT/DCA.\n");
        break;

      case 'G': // Global volume
        instruments[n].heading[7]=x=read_int(&s);
        if(x<0 || x>128) fprintf(stderr,"Abnormal instrument global volume.\n");
        break;

      case 'R': // Random volume/panning variation
        instruments[n].heading[9]=x=read_int(&s);
        if(x<0 || x>100) fprintf(stderr,"Abnormal random volume variation.\n");
        instruments[n].heading[10]=read_int(&s);
        break;

      case 'Y': // Pitch-pan separation/center
        x=read_int(&s);
        if(x<-32 || x>32) fprintf(stderr,"Abnormal pitch-pan separation.\n");
        instruments[n].heading[5]=x;
        if(!(x=*s++)) {
          fprintf(stderr,"Instrument definition ends in a pitch-pan specification.\n");
          return;
        }
        if(x=='n') {
          x=read_int(&s);
        } else {
          if(x<'a' || x>'j') {
            fprintf(stderr,"*FATAL* Invalid pitch-pan center note letter.\n");
            exit(1);
          }
          x=(x<scale_base)*octave_tones+note_letter[x-'a'];
          while(*s=='+' || *s=='-') x+=*s++=='+'?:-1;
          x+=octave_tones*read_int(&s);
        }
        if(x<0 || x>119) fprintf(stderr,"Abnormal pitch-pan center.\n");
        instruments[n].heading[6]=x;
        break;

      case 'c': // Initial filter cutoff
        instruments[n].ifc=(x=read_int(&s))|128;
        if(x<0 || x>127) fprintf(stderr,"Abnormal initial filter cutoff.\n");
        break;

      case 'f': // Filter envelope
        instruments[n].pitch_env[0]|=0x80;
        read_envelope(instruments[n].pitch_env,&s);
        break;

      case 'k': // Key/sample table
        read_instrument_key_table(n,&s);
        break;

      case 'i': // Pitch envelope
        instruments[n].pitch_env[0]&=~0x80;
        read_envelope(instruments[n].pitch_env,&s);
        break;

      case 'p': // Pan envelope
        read_envelope(instruments[n].pan_env,&s);
        break;

      case 'r': // Initial filter resonance
        instruments[n].ifr=(x=read_int(&s))|128;
        if(x<0 || x>127) fprintf(stderr,"Abnormal initial filter resonance.\n");
        break;

      case 'v': // Volume envelope
        read_envelope(instruments[n].vol_env,&s);
        break;

      case 'y': // Default pan
        instruments[n].heading[8]=x=read_int(&s);
        if(x<0 || x>64) fprintf(stderr,"Abnormal instrument default pan.\n");
        break;

      case '}': // End of instrument definition
        skip_spaces(&s);
        if(*s) fprintf(stderr,"Instrument definition ends early.\n");
        return;

      default:
        fprintf(stderr,"Unknown command in instrument definition: %c\n",s[-1]);
    }
  }
}

void read_key_table_def(byte n,char*s) {
  char*t;
  int k=0;
  int x;
  char*loop=0;
  while(skip_spaces(&s),*s) {
    switch(*s++) {

      case '*': // Volume
        keytab[n][k].volumeset=read_int(&s);
        break;

      case '@': // Adjust variable/direct effects/instrument/copy key table/cusfom effect sequence
        if(*s=='=') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]=read_int(&s);
        } else if(*s=='+') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]+=read_int(&s);
        } else if(*s=='-') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]-=read_int(&s);
        } else if(*s=='*') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]*=read_int(&s);
        } else if(*s=='e') {
          s++; keytab[n][k].effectid=read_int(&s); keytab[n][k].effectpar=read_int(&s); keytab[n][k].volumeset=read_int(&s);
        } else if((*s>='0' && *s<='9') || *s=='$') {
          keytab[n][k].instrument=read_int(&s)?:255;
          if(keytab[n][k].instrument>max_instrument) fprintf(stderr,"Unuse instrument %d.\n",channels[ch].instrument);
        } else if(*s=='@') {
          t=++s; x=read_int(&s);
          if(x!=n && x>=0 && x<64) memcpy(keytab[n]+k,keytab[x]+k,sizeof(key_setting));
          else fprintf(stderr,"Wrong key table number %d.\n",x);
        } else if(*s=='u') {
          t=++s; x=read_int(&s); keytab[n][k].customfx=(t==s)?255:x;
        } else if(*s=='`') {
          *numeric_register=at_backtick(n,k,*++s);
        } else {
          fprintf(stderr,"Unknown command in channel %c: @%c\n", ch+'A', *s++);
        }
        break;

      case '[': // Begin loop
        if(loop) fprintf(stderr,"Nested loop in key table definition.\n");
        loop=s;
        break;

      case ']': // End loop
        x=read_note(&s);
        if(x<0 || x>119) x=119;
        if(loop) {
          if(k<x) {
            k++;
            s=loop;
          } else {
            k=0;
            loop=0;
          }
        } else {
          fprintf(stderr,"Loop end too much in key table definition.\n");
        }
        break;

      case 'I': // Tremor
        keytab[n][k].effectid=FX('I');
        keytab[n][k].effectpar=read_int(&s)<<4;
        keytab[n][k].effectpar|=read_int(&s);
        break;

      case 'J': // Arpeggio
        keytab[n][k].effectid=FX('J');
        keytab[n][k].effectpar=read_int(&s)<<4;
        keytab[n][k].effectpar|=read_int(&s);
        break;

      case 'K': // Transpose
        keytab[n][k].note=k+read_int(&s);
        break;

      case 'Q': // Retrigger
        keytab[n][k].effectid=FX('Q');
        keytab[n][k].effectpar=read_int(&s)<<4;
        keytab[n][k].effectpar|=read_int(&s);
        break;

      case 'R': // Portamento
        x=read_int(&s);
        if(x<-0xDF || !x || x>0xDF) {
          fprintf(stderr,"Abnormal portamento rate.\n");
        } else if(x>0) {
          keytab[n][k].effectid=FX('F');
          keytab[n][k].effectpar=x;
        } else {
          keytab[n][k].effectid=FX('E');
          keytab[n][k].effectpar=-x;
        }
        break;

      case 'T': // Tremolo
        keytab[n][k].effectid=FX('R');
        keytab[n][k].effectpar=read_int(&s)<<4;
        keytab[n][k].effectpar|=read_int(&s);
        break;

      case 'V': // Vibrato
        x=read_int(&s);
        if(x<1 || x>15) fprintf(stderr,"Abnormal vibrato speed.\n");
        keytab[n][k].effectpar=x<<4;
        x=read_int(&s);
        if(x>0 && x<16) {
          keytab[n][k].effectid=FX('U');
          keytab[n][k].effectpar|=x;
        } else if(x>0 && x<=60 && !(x&3)) {
          keytab[n][k].effectid=FX('H');
          keytab[n][k].effectpar|=x>>2;
        } else {
          fprintf(stderr,"Abnormal vibrato depth.\n");
        }
        break;

      case 'Y': // Panbello
        keytab[n][k].effectid=FX('Y');
        keytab[n][k].effectpar=read_int(&s)<<4;
        keytab[n][k].effectpar|=read_int(&s);
        break;

      case 'a' ... 'j': case 'n': // Note entry
        s--;
        k=read_note(&s);
        if(k<0 || k>119) {
          fprintf(stderr,"Note out-of-range.\n");
          k=0;
        }
        break;

      case '}': // End of key table definition
        skip_spaces(&s);
        if(*s) fprintf(stderr,"Key table definition ends early.\n");
        return;

      default:
        fprintf(stderr,"Unknown command in key table definition: %c\n",s[-1]);
    }
  }
}

void read_custom_fx_def(byte n,char*s) {
  custom_fx*cfx=fxtab+n;
  custom_fx*tmp;
  short cuts=255;
  byte cont=0; // 0x80=continue sequence before/after note stopped
  int x;
  char*loop=0;
  int loopc=0;
  while(skip_spaces(&s),*s) {
    switch(*s++) {

      case '-': case '+': case '$': case '0' ... '9': // Delta
        s--;
        cfx->delta=read_int(&s);
        tmp=cfx->next;
        cfx->next=malloc(sizeof(custom_fx));
        cfx=cfx->next;
        cfx->transpose=0;
        cfx->instrument=255;
        cfx->effectid=0;
        cfx->volumeset=255;
        cfx->continue_flag=cont;
        cfx->delta=0;
        cfx->next=tmp;
        break;

      case '(': // Begin continue sequence
        cfx->continue_flag|=(cont=0x80);
        break;

      case ')': // End continue sequence
        cont=0;
        break;

      case '*': // Note volume
        cfx->volumeset=read_int(&s);
        cfx->continue_flag&=~0x02;
        break;

      case ':': // Final delta
        cfx->delta=read_int(&s);
        break;

      case '@': // Direct effect entry, chain to sequence, instrument select
        if(*s=='=') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]=read_int(&s);
        } else if(*s=='+') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]+=read_int(&s);
        } else if(*s=='-') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]-=read_int(&s);
        } else if(*s=='*') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]*=read_int(&s);
        } else if(*s=='e') {
          s++; cfx->effectid=read_int(&s); cfx->effectpar=read_int(&s); cfx->volumeset=read_int(&s);
          cfx->continue_flag|=0x03;
        } else if(*s=='f') {
          s++; cfx->effectid=read_int(&s); cfx->effectpar=read_int(&s);
          cfx->continue_flag&=~0x05;
        } else if(*s=='u') {
          s++; cfx->next=fxtab+(63&read_int(&s));
        } else if(*s=='v') {
          s++; cfx->volumeset=read_int(&s);
          cfx->continue_flag&=~0x02;
        } else if(*s=='z') {
          cfx->continue_flag|=0x04;
        } else if((*s>='0' && *s<='9') || *s=='$') {
          cfx->instrument=read_int(&s);
        } else {
          fprintf(stderr,"Unknown command in custom effect sequence: @%c\n", *s++);
        }
        break;

      case 'C': // Set note cuts
        x=*s++;
        if(x=='o') cfx->transpose=cuts=255;
        else if(x=='c') cfx->transpose=cuts=254;
        else if(x=='f') cfx->transpose=cuts=253;
        else fprintf(stderr,"Unknown command in custom effect sequence: C%c\n",x);
        break;

      case 'I': // Tremor
        cfx->effectid=FX('I');
        cfx->effectpar=read_int(&s)<<4;
        cfx->effectpar|=read_int(&s);
        cfx->continue_flag|=0x01;
        break;

      case 'J': // Arpeggio
        cfx->effectid=FX('J');
        cfx->effectpar=read_int(&s)<<4;
        cfx->effectpar|=read_int(&s);
        cfx->continue_flag|=0x01;
        break;

      case 'K': // Transpose
        cfx->transpose=read_int(&s);
        break;

      case 'Q': // Retrigger
        cfx->effectid=FX('Q');
        cfx->effectpar=read_int(&s)<<4;
        cfx->effectpar|=read_int(&s);
        cfx->continue_flag|=0x01;
        break;

      case 'R': // Portamento
        x=read_int(&s);
        if(x<-0xDF || !x || x>0xDF) {
          fprintf(stderr,"Abnormal portamento rate.\n");
        } else if(x>0) {
          cfx->effectid=FX('F');
          cfx->effectpar=x;
        } else {
          cfx->effectid=FX('E');
          cfx->effectpar=-x;
        }
        cfx->continue_flag|=0x01;
        break;

      case 'T': // Tremolo
        cfx->effectid=FX('R');
        cfx->effectpar=read_int(&s)<<4;
        cfx->effectpar|=read_int(&s);
        cfx->continue_flag|=0x01;
        break;

      case 'V': // Vibrato
        x=read_int(&s);
        if(x<1 || x>15) fprintf(stderr,"Abnormal vibrato speed.\n");
        cfx->effectpar=x<<4;
        x=read_int(&s);
        if(x>0 && x<16) {
          cfx->effectid=FX('U');
          cfx->effectpar|=x;
        } else if(x>0 && x<=60 && !(x&3)) {
          cfx->effectid=FX('H');
          cfx->effectpar|=x>>2;
        } else {
          fprintf(stderr,"Abnormal vibrato depth.\n");
        }
        cfx->continue_flag|=0x01;
        break;

      case 'Y': // Panbello
        cfx->effectid=FX('Y');
        cfx->effectpar=read_int(&s)<<4;
        cfx->effectpar|=read_int(&s);
        cfx->continue_flag|=0x01;
        break;

      case '[': // Repeat begin
        if(loop) fprintf(stderr,"Prohibit nesting repeats.\n");
        loop=s;
        loopc=0;
        break;

      case ']': // Repeat end
        if(!loop) {
          fprintf(stderr,"*FATAL* Repeat block ends without starting.\n");
          exit(1);
        }
        if((x=read_int(&s))<0) fprintf(stderr,"Abnormal repeat count.\n");
        if(x==++loopc) loop=0; else s=loop;
        break;

      case 'r': // Rest
        cfx->transpose=cuts;
        break;

      case 'w': // Wait
        cfx->transpose=200;
        break;

      case '|': // Loop mark
        cfx->next=cfx;
        break;

      case '}': // End of definition
        skip_spaces(&s);
        if(*s) fprintf(stderr,"Custom effect sequence definition ends early.\n");
        goto check_custom_fx;

      default:
        fprintf(stderr,"Unknown command in custom effect sequence definition: %c\n",s[-1]);
    }
  }
check_custom_fx:
  if(!cfx->next) return;
  //todo
}

void at_definition(char*s) {
  int x,y;
  char*t;
  char*p;
  switch(*s++) {

    case '0' ... '9': // Instrument
    case '$': // Instrument (hex)
      s--;
      x=read_int(&s);
      if(x<=0 || x>99) {
        fprintf(stderr,"Wrong instrument number (%d).\n",x);
        return;
      }
      if(max_instrument<x) max_instrument=x;
      skip_equals_brace(&s);
      read_instrument_def(x,s);
      break;

    case '@': // Key table
      x=read_int(&s);
      if(x<0 || x>63) {
        fprintf(stderr,"Wrong key table number (%d).\n",x);
        return;
      }      
      skip_equals_brace(&s);
      read_key_table_def(x,s);
      break;

    case 'A' ... 'P': // Channel setting
      x=s[-1]-'A';
      skip_equals_brace(&s); channels[x].ch_vol=read_int(&s);
      skip_spaces(&s); channels[x].ch_pan=read_int(&s);
      if(channels[x].ch_vol<0 || channels[x].ch_vol>64) fprintf(stderr,"Abnormal channel volume (%c).\n",x+'A');
      if((channels[x].ch_pan<0 || channels[x].ch_pan>64) && channels[x].ch_pan!=100)
        fprintf(stderr,"Abnormal channel panning (%c).\n",x+'A');
      break;

    case 'X': // Text macro
      x=read_int(&s);
      if(x&~0xFF) {
        fprintf(stderr,"Wrong macro number (%d).\n",x);
        return;
      }
      t=s; y=(*t>='A' && *t<='P');
      while(*s && *s!='=') s++;
      if(!*s) {
        fprintf(stderr,"Incomplete macro definition (%d).\n",x);
        return;
      }
      s++; skip_spaces(&s); p=strdup(s);
      if(y) {
        while(*t>='A' && *t<='P') macro_text[x][*t++-'A']=p;
      } else {
        while(y<16) macro_text[x][y++]=p;
      }
      break;

    case 's': // Sample
      x=read_int(&s);
      if(x<=0 || x>99) {
        fprintf(stderr,"Wrong sample number (%d).\n",x);
        return;
      }
      if(max_sample<x) max_sample=x;
      samples[x].flag=1; // sample associated with header
      skip_equals_brace(&s);
      read_sample_def(x,s);
      break;

    case 'u': // Custom effect sequence
      x=read_int(&s);
      if(x<0 || x>63) {
        fprintf(stderr,"Wrong custom effect sequence number (%d).\n",x);
        return;
      }      
      skip_equals_brace(&s);
      read_custom_fx_def(x,s);
      break;

    default:
      fprintf(stderr,"Unknown command: @%c\n",*s);
  }
}

void send_note(byte ch,short no,int prelen,char**s) {
  short dot;
  short quan;
  int len;
  byte vol=255;
  byte inst=channels[ch].instrument;
  byte fxid=channels[ch].effectid;
  byte fxpar=channels[ch].effectpar;
  byte volfx=channels[ch].volumeset;
  byte cusfx=channels[ch].customfx;
  custom_fx*cfx;
  int x;
  boolean contfx=0;
  channels[ch].row_tie=channels[ch].row;
  if(s) {
    while(**s) {
      if(**s=='\'') no+=octave_tones;
      else if(**s=='"') no+=octave_tones*2;
      else if(**s=='-') no--;
      else if(**s=='+') no++;
      else break;
      (*s)++;
    }
    len=read_int(s);
    if(!len) len=channels[ch].length?:1;
    if(whole_note%len) fprintf(stderr,"Inexact note length (%d).\n",(int)len);
    dot=len=whole_note/len;
    while(**s) {
      if(**s=='.') len+=(dot/=2);
      else break;
      (*s)++;
    }
    if(**s=='*') {
      (*s)++; vol=read_int(s);
    }
  }
  if(!dot) fprintf(stderr,"Inexact note length.\n");
  if(channels[ch].quantize>0) {
    len-=quan=(len*channels[ch].quantize)/gate_denom;
  } else {
    len-=quan=-channels[ch].quantize;
  }
  len+=prelen;
  if(len<1) {
    len=1;
    quan=0;
    fprintf(stderr,"Quantize results in too short note.\n");
  }
  if(channels[ch].keytable!=255) {
    x=channels[ch].keytable;
    inst=keytab[x][no].instrument==255?inst:keytab[x][no].instrument;
    fxid=keytab[x][no].effectid?:fxid;
    fxpar=keytab[x][no].effectid?keytab[x][no].effectpar:fxpar;
    volfx=keytab[x][no].volumeset==255?volfx:keytab[x][no].volumeset;
    cusfx=keytab[x][no].customfx==255?cusfx:keytab[x][no].customfx;
    no=keytab[x][no].note;
  }
  if(volfx!=255 && vol!=255 && vol!=volfx) fprintf(stderr,"Conflicting volume effects during note.\n");
  volfx=vol;
  cfx=cusfx==255?0:fxtab+cusfx;
  if(cfx && no<200) {
    while(cfx && (len || contfx)) {
      contfx=!!(cfx->continue_flag&0x80);
      if(cfx->transpose<200) no+=cfx->transpose;
      if(no<0 || no>119) {
        no=0; fprintf(stderr,"Custom effect table causes note out of range error.\n");
      }
      x=cfx->delta;
      if(x>len && !contfx) x=len;
      send_channel(ch,cfx->transpose>=200?cfx->transpose:no,(cfx->instrument==255 && cfx->transpose<200255)?inst:cfx->instrument,
       cfx->effectid,cfx->effectpar,cfx->volumeset,cfx->continue_flag,x);
      cfx=cfx->next;
      len-=x;
    }
    channels[ch].row+=len;
  } else {
    send_channel(ch,no,inst,fxid,fxpar,volfx,1,len);
  }
  //todo: auto portamento
  channels[ch].row_cut=channels[ch].row;
  if(quan) send_channel(channels[ch].note_cut,no,inst,255,0,volfx,0,quan);
  channels[ch].auto_portamento=0;
}

void process_channel(byte ch,char*s) {
  char c;
  char*t;
  int x;
  char*loop=0;
  int loopc=0;
  skip_spaces(&s);
  while(*s) {
    switch(c=*s++) {

      case 'a' ... 'j': // Notes
        send_note(ch,channels[ch].transpose+(channels[ch].octave+(c<scale_base))*octave_tones+note_letter[c-'a'],0,&s);
        break;

      case 'r': // Rest
        send_note(ch,channels[ch].note_cuts,0,&s);
        break;

      case 'n': // Direct note
        send_note(ch,read_int(&s)+channels[ch].transpose,0,&s);
        break;

      case '^': // Tie
        if(channels[ch].row_cut<max_frames) frames[channels[ch].row_cut].data[ch].continue_flag=0;
        x=channels[ch].row-channels[ch].row_tie;
        channels[ch].row=channels[ch].row_tie;
        send_note(ch,channels[ch].prev_note,x,&s);
        break;

      case '&': // Slur
        if(channels[ch].row_cut<max_frames) frames[channels[ch].row_cut].data[ch].continue_flag=0;
        break;

      case '/': // Auto portamento
        channels[ch].auto_portamento=1;
        break;

      case 'L': // Loop restart point
        allocate_frames(channels[ch].row+1);
        frames[channels[ch].row].used=1;
        channels[ch].loopstart=channels[ch].row;
        break;

      case '|': // Go back to loop restart, or skip if not looping
        c=*s++;
        if(c=='|') { 
          allocate_frames(channels[ch].row+1);
          frames[channels[ch].row].used=1;
          channels[ch].loopskip=channels[ch].row;
        } else if(c=='L') {
          allocate_frames(channels[ch].row+1);
          frames[channels[ch].row].used=1;
          channels[ch].loopend=channels[ch].row;
        } else if(c==':') {
          //todo (begin runtime repeat block)
        } else {
          fprintf(stderr,"Unknown command in channel %c: |%c\n", ch+'A', c);
        }
        break;

      case ':': // Runtime repeat block end/entry point by order number
        c=*s++;
        if(c=='|') {
          //todo (end runtime repeat block)
        } else if(c=='$' || (c>='0' && c<='9')) {
          //todo (entry point by order number)
        } else {
          fprintf(stderr,"Unknown command in channel %c: :%c\n", ch+'A', c);
        }
        break;

      case '@': // Set instrument or key table or quantize or other channel or etc
        if(*s=='{' || *s=='}') {
          s++; // for use with text macros
        } else if(*s=='=') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]=read_int(&s);
        } else if(*s=='+') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]+=read_int(&s);
        } else if(*s=='-') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]-=read_int(&s);
        } else if(*s=='*') {
          s++; x=(*s++&0x1F)%27; numeric_register[x]*=read_int(&s);
        } else if(*s=='q') {
          s++; channels[ch].quantize=-read_int(&s);
        } else if(*s=='e') {
          s++; channels[ch].effectid=read_int(&s); channels[ch].effectpar=read_int(&s); channels[ch].volumeset=read_int(&s);
        } else if(*s>='A' && *s<='P') {
          ch=*s++-'A';
        } else if((*s>='0' && *s<='9') || *s=='$') {
          channels[ch].instrument=read_int(&s); channels[ch].keytable=255;
          if(channels[ch].instrument>max_instrument) fprintf(stderr,"Unuse instrument %d.\n",channels[ch].instrument);
        } else if(*s=='@') {
          t=++s; x=read_int(&s); channels[ch].keytable=(t==s)?255:x;
        } else if(*s=='u') {
          t=++s; x=read_int(&s); channels[ch].customfx=(t==s)?255:x;
        } else if(*s=='`') {
          *numeric_register=at_backtick(ch,loopc,*++s);
        } else {
          fprintf(stderr,"Unknown command in channel %c: @%c\n", ch+'A', *s++);
        }
        break;

      case 'o': // Octave set
        channels[ch].octave=read_int(&s);
        if(channels[ch].octave>9) fprintf(stderr,"Abnormal octave number (o).\n");
        break;

      case '<': // Low octave
        channels[ch].octave-=octave_dir;
        if(channels[ch].octave>9) fprintf(stderr,"Abnormal octave number (<).\n");
        break;

      case '>': // High octave
        channels[ch].octave+=octave_dir;
        if(channels[ch].octave>9) fprintf(stderr,"Abnormal octave number (>).\n");
        break;

      case 'l': // Length
        channels[ch].length=read_int(&s);
        if(channels[ch].length<=0 || whole_note%channels[ch].length)
         fprintf(stderr,"Abnormal note length (%d/%d).\n",whole_note,channels[ch].length);
        break;

      case 'K': // Transpose
        channels[ch].transpose=read_int(&s);
        break;

      case 'v': // Volume
        send_channel(ch,200,FX('M'),read_int(&s),0,255,0,0);
        break;

      case 'G': // Global volume
        send_global(channels[ch].row,FX('V'),read_int(&s),255,0,1);
        break;

      case 'X': // Call macro
        x=read_int(&s); skip_spaces(&s);
        if(x&~0xFF) {
          fprintf(stderr,"Wrong macro number (%d).\n",x);
          break;
        }
        if(*s) {
          process_channel(ch,macro_text[x][ch]);
        } else {
          s=macro_text[x][ch]; // tail recursive
        }
        break;

      case 'C': // Set note cuts
        x=*s++;
        if(x=='o') channels[ch].note_cuts=255;
        else if(x=='c') channels[ch].note_cuts=254;
        else if(x=='f') channels[ch].note_cuts=253;
        else fprintf(stderr,"Unknown command in channel %c: C%c\n",ch+'A',x);
        break;

      case 'q': // Set quantize
        channels[ch].quantize=read_int(&s);
        break;

      case 't': // Set tempo
        send_global(channels[ch].row,FX('T'),read_int(&s),255,0,1);
        break;

      case '[': // Repeat begin
        if(loop) fprintf(stderr,"Prohibit nesting repeats.\n");
        loop=s;
        loopc=0;
        break;

      case ']': // Repeat end
        if(!loop) {
          fprintf(stderr,"*FATAL* Repeat block ends without starting.\n");
          exit(1);
        }
        if((x=read_int(&s))<0) fprintf(stderr,"Abnormal repeat count.\n");
        if(x==++loopc) loop=0; else s=loop;
        break;

      case 'I': // Tremor
        channels[ch].effectid=FX('I');
        channels[ch].effectpar=read_int(&s)<<4;
        channels[ch].effectpar|=read_int(&s);
        break;

      case 'J': // Arpeggio
        channels[ch].effectid=FX('J');
        channels[ch].effectpar=read_int(&s)<<4;
        channels[ch].effectpar|=read_int(&s);
        break;

      case 'T': // Tremolo
        channels[ch].effectid=FX('R');
        channels[ch].effectpar=read_int(&s)<<4;
        channels[ch].effectpar|=read_int(&s);
        break;

      case 'P': // Panning (volume column)
        channels[ch].volumeset=read_int(&s)|128;
        break;

      case 'Q': // Retrigger
        channels[ch].effectid=FX('Q');
        channels[ch].effectpar=read_int(&s)<<4;
        channels[ch].effectpar|=read_int(&s);
        break;

      case 'V': // Vibrato
        x=read_int(&s);
        if(x<1 || x>15) fprintf(stderr,"Abnormal vibrato speed.\n");
        channels[ch].effectpar=x<<4;
        x=read_int(&s);
        if(x>0 && x<16) {
          channels[ch].effectid=FX('U');
          channels[ch].effectpar|=x;
        } else if(x>0 && x<=60 && !(x&3)) {
          channels[ch].effectid=FX('H');
          channels[ch].effectpar|=x>>2;
        } else {
          fprintf(stderr,"Abnormal vibrato depth.\n");
        }
        break;

      case 'Y': // Panbrello
        channels[ch].effectid=FX('Y');
        channels[ch].effectpar=read_int(&s)<<4;
        channels[ch].effectpar|=read_int(&s);
        break;

      case 'R': // Portamento
        x=read_int(&s);
        if(x<-0xDF || !x || x>0xDF) {
          fprintf(stderr,"Abnormal portamento rate.\n");
        } else if(x>0) {
          channels[ch].effectid=FX('F');
          channels[ch].effectpar=x;
        } else {
          channels[ch].effectid=FX('E');
          channels[ch].effectpar=-x;
        }
        break;

      case 'z': // Cancel effects
        channels[ch].effectid=0;
        channels[ch].volumeset=255;
        break;

      case '\\': // Synchronize channel
        x=*s++-'A';
        if(x<0 || x>15) {
          fprintf(stderr,"Cannot synchronize wrong channel.\n");
        } else {
          channels[ch].row=channels[x].row;
          channels[ch].row_cut=channels[x].row_cut;
          channels[ch].row_tie=channels[x].row_tie;
          channels[ch].loopstart=channels[x].loopstart;
          channels[ch].loopskip=channels[x].loopskip;
          channels[ch].loopend=channels[x].loopend;
        }
        break;

      default:
        fprintf(stderr,"Unknown command in channel %c: %c\n", ch+'A', c);
    }
    skip_spaces(&s);
  }
  if(loop) fprintf(stderr,"Repeat block not ended (repeat is ignored).\n");
}

void process_line(char*s) {
  char*t;
  switch(*s) {
    case 0: case ';': break;
    case '"':
      *next_song_message=malloc(sizeof(message_line));
      (*next_song_message)->data=strdup(s+1);
      (*next_song_message)->next=0;
      next_song_message=&((*next_song_message)->next);
      song_message_length+=strlen(s); // Includes the '"' which is the same length as newline marker
      if(song_message_length>8000) fprintf(stderr,"Overlong song message.\n");
      break;
    case '#': hash_directive(s+1); break;
    case '@': at_definition(s+1); break;
    case 'A' ... 'P':
      for(t=s;0x40&*t;t++);
      while(*s>='A' && *s<='P') process_channel(*s++-'A',t);
      break;
    default:
      fprintf(stderr,"Illegal character at start of line: %c\n", *s);
  }
}

void read_file(FILE*fp) {
  char buf[4096];
  int bp=0; // buffer mark
  int ns=0; // not space
  int c;
  boolean bl=1; // 0 to block of multi lines
  boolean com=1; // 0 to comment out
  *buf=0;
  while(!feof(fp)) {
    if((c=fgetc(fp))==EOF) break;

    if(com) {
      if(c>' ') ns=bp+1;
      if(c>' ' || bp) {
        buf[bp++]=c>' '?c:' ';
        if(bp>=4096) {
          fprintf(stderr,"*FATAL* Overlong line of input.\n");
          exit(1);
        }
      }
      if(bp) buf[bp-1]*=(com=(*buf<36 || c!=';')); // cut off comment
      if(*buf=='@') {
        if(c=='{') bl=0;
        if(c=='}') bl=1;
      }
    }

    if(c=='\r' || c=='\n') {
      com=1;
      if(bl && bp) {
        buf[ns]=0;
        if(trace_input) fprintf(stderr,"+ %s\n",buf);
        process_line(buf);
        bp=ns=0;
      }
    }

  }
  if(trace_input) fprintf(stderr,"<EOF>\n");
}

void make_defaults(void) {
  int x,y;
  for(x=1;x<100;x++) {
    // Sample
    samples[x].global_volume=64;
    samples[x].flag=0x00;
    samples[x].default_volume=64;
    samples[x].convert=0x00;
    samples[x].pan=64;
    samples[x].length=samples[x].loop_start=samples[x].loop_end=0;
    samples[x].c5speed=8363;
    samples[x].sustain_start=samples[x].sustain_end=0;
    samples[x].make=0;
    samples[x].name=0;
    samples[x].used=0;
    // Instrument
    for(y=0;y<11;y++) instruments[x].heading[y]=0;
    instruments[x].heading[6]=60;
    instruments[x].heading[7]=0x40;
    instruments[x].heading[8]=0xA0;
    instruments[x].ifc=instruments[x].ifr=0;
    for(y=0;y<120;y++) {
      instruments[x].keyboard[y<<1]=y;
      instruments[x].keyboard[1|(y<<1)]=0;
    }
    instruments[x].used=0;
    instruments[x].vol_env[0]=instruments[x].vol_env[1]=0;
    instruments[x].pan_env[0]=instruments[x].pan_env[1]=0;
    instruments[x].pitch_env[0]=instruments[x].pitch_env[1]=0;
  }
  for(x=0;x<64;x++) {
    // Key table
    for(y=0;y<120;y++) {
      keytab[x][y].note=y;
      keytab[x][y].effectid=0;
      keytab[x][y].instrument=keytab[x][y].volumeset=keytab[x][y].customfx=255;
    }
    // Effect table
    fxtab[x].transpose=0;
    fxtab[x].instrument=255;
    fxtab[x].effectid=0;
    fxtab[x].volumeset=255;
    fxtab[x].continue_flag=0;
    fxtab[x].delta=0;
    fxtab[x].next=0;
  }
  for(x=0;x<16;x++) {
    // Channel
    channels[x].transpose=0;
    channels[x].octave=5;
    channels[x].length=4;
    channels[x].instrument=1;
    channels[x].effectid=0;
    channels[x].volumeset=255;
    channels[x].quantize=0;
    channels[x].note_cuts=255;
    channels[x].ch_vol=64;
    channels[x].ch_pan=32;
    channels[x].row=channels[x].row_cut=channels[x].row_tie=0;
    channels[x].loopstart=channels[x].loopskip=channels[x].loopend=-1;
    channels[x].keytable=255;
    channels[x].customfx=255;
    channels[x].auto_portamento=0;
    channels[x].prev_note=254;
  }
}

byte*synthesizer_a(byte n) {
  char*cmd=samples[n].name+1;
  int per=lround(((double)samples[n].c5speed)/C_pitch); // period (before multiply)
  int len=lround(((double)(samples[n].c5speed*synth_len_mult))/C_pitch);
  double*dbl;
  byte*buf;
  int x,z1,z2,z3;
  double q,r;
  double amp=1.0;

  // Amplitude/length
  x=read_int(&cmd)?:1000; amp=0.001*(double)x;
  len*=read_int(&cmd)?:1;
  dbl=malloc(len*sizeof(double));
  buf=malloc(len<<synth16bits);

  // Set samples
  samples[n].length=samples[n].loop_end=len;
  samples[n].flag|=0x10;
  samples[n].convert=0x00;
  samples[n].loop_start=0;

  // Reset buffer
  for(x=0;x<len;x++) dbl[x]=0;

  // Calculation
  while(*cmd) {
    skip_spaces(&cmd);
    if(*cmd=='[') cmd++;
    if(!*cmd) break;
    if(*cmd=='L') {
      // square wave
      cmd++;
      z1=read_int(&cmd); // duty
      z2=read_int(&cmd); // amplitude
      if(!z2) {
        fprintf(stderr,"*FATAL* Improper synthesizer for sample %d.\n",n);
        exit(1);
      }
      for(x=0;x<len;x++) dbl[x]=(x%per<(z1*per)/1000)?(1.0/(double)z2):(dbl[x]-1.0/(double)z2);
    } else if(*cmd=='N') {
      // saw wave
      cmd++;
      z1=read_int(&cmd); // amplitude
      if(!z1) {
        fprintf(stderr,"*FATAL* Improper synthesizer for sample %d.\n",n);
        exit(1);
      }
      q=1.0/(double)z1;
      for(x=0;x<len;x++) {
        r=q*(double)(x%per-per/2);
        dbl[x]=fabs(dbl[x])<r?r:dbl[x];
      }
    } else if(*cmd=='V') {
      // triangle wave
      cmd++;
      //todo
    } else if(*cmd=='R') {
      // random noise
      cmd++;
      z1=read_int(&cmd); // divider
      z2=read_int(&cmd); // amplitude      
      z3=read_int(&cmd); // exponent
      if(!z3) z3=1;
      if(z1<=0 || !z2) {
        fprintf(stderr,"*FATAL* Improper synthesizer for sample %d.\n",n);
        exit(1);
      }
      for(x=0;x<len;x++) {
        if(!(x%z1)) q=pow(rand_double(),z3)/(double)z2;
        dbl[x]+=q;
      }
    } else if(0x5F&*cmd) {
      // sine wave
      z1=read_int(&cmd); // frequency
      z2=read_int(&cmd); // amplitude
      z3=read_int(&cmd); // phase
      q=(tau*(double)z3)/1000.0;
      if(!z2) {
        fprintf(stderr,"*FATAL* Improper synthesizer for sample %d.\n",n);
        exit(1);
      }
      for(x=0;x<len;x++) dbl[x]+=sin((q+tau*(double)(z1*x))/per)/(double)z2;
    }
  }

  // Return byte buffer
  if(synth16bits) {
    samples[n].flag|=0x02;
    samples[n].convert|=0x01;
    for(x=0;x<len;x++) {
      dbl[x]*=amp;
      z1=lround(dbl[x]*0x8000);
      if(z1<-0x7FFF) z1=-0x7FFF;
      if(z1>0x7FFF) z1=0x7FFF;
      buf[x<<1]=z1;
      buf[1|(x<<1)]=z1>>8;
    }
  } else {
    for(x=0;x<len;x++) {
      dbl[x]*=amp;
      z1=lround((dbl[x]+1)*128);
      if(z1>255) z1=255;
      if(z1<0) z1=0;
      buf[x]=z1;
    }
  }
  free(dbl);
  return buf;
}

byte*synthesizer_b(byte n) {
  char*cmd=samples[n].name+1;
  byte*buf=malloc(0x5000); // limit=0x4000
  int len=0;
  int loops[16];
  int slows[16];
  int loopnest=0;
  int slow=1;
  int x,y,z;

  // Read commands and make buffer
  while(*cmd) {
    skip_spaces(&cmd);
    if(*cmd=='[') cmd++;
    if(!*cmd) break;
    if(*cmd=='|') {
      // loop point
      cmd++;
      samples[n].flag|=0x10;
      samples[n].loop_start=len;
    } else if(*cmd=='(' || *cmd=='$' || (*cmd>='0' && *cmd<='9') || *cmd=='-' || *cmd=='+') {
      x=read_int(&cmd);
      if(*cmd=='(') {
        if(loopnest==15) {
          fprintf(stderr,"*FATAL* Too many nested repeats in sample %d.\n",n);
          exit(1);
        }
        loops[loopnest]=len>>synth16bits;
        slows[loopnest++]=slow;
        slow*=x?:1;
      } else {
        y=slow;
        while(y--) {
          buf[len++]=x;
          if(synth16bits) buf[len++]=x>>8;
        }
      }
    } else if(*cmd==')') {
      cmd++;
      if(loopnest) {
        x=read_int(&cmd)?:1;
        z=len;
        --loopnest;
        while(x--) {
          y=loops[loopnest];
          while(y<z) buf[len++]=buf[y++];
        }
        slow=slows[loopnest];
      } else {
        fprintf(stderr,"Stack underflow in sample %d.\n",n);
      }
    } else if(*cmd>' ') {
      cmd++;
      fprintf(stderr,"Improper synthesizer for sample %d.\n",n);
    }
    if(len>=0x3FFF) {
      fprintf(stderr,"*FATAL* Overlong sample %d.\n",n);
      exit(1);
    }
  }

  // Send buffer
  samples[n].convert=0x00;
  samples[n].length=samples[n].loop_end=len>>synth16bits;
  if(synth16bits) {
    samples[n].flag|=0x02;
    samples[n].convert|=0x01;
  }
  return buf;
}

void do_export_sample(void) {
  sample_data*sam=samples+export_sample;
  byte*buf;
  int x;
  if(export_sample<=max_sample && sam->make==2) {
    if(sam->name[0]=='a') buf=synthesizer_a(export_sample);
    if(sam->name[0]=='b') buf=synthesizer_b(export_sample);
    fwrite(buf,1,sam->length<<synth16bits,stdout);
    free(buf);
  } else {
    fprintf(stderr,"*FATAL* Cannot export sample %d to stdout.\n",export_sample);
    exit(1);
  }
}

//=========================================//

typedef struct pat_row {
  byte note[18]; // 200=unuse
  byte instrument[18]; // 255=unuse
  byte volume[18]; // 255=unuse
  byte effectid[18]; // 255=unuse (ch.16=globals, ch.17=speed, ch.18=break)
  byte effectpar[18];
  struct pat_row*next;
} pat_row;

pat_row*pat_start;
pat_row*pat_loop_begin;
pat_row*pat_loop_skip;
pat_row*pat_loop_end;
byte*pat_data;
int where_loopstart_ord; // offset +pat_data
int where_loopskip_ord;
int where_loopend_ord;
byte pattern_loopstart=255; // pattern number
byte pattern_loopskip=255;
byte pattern_loopend=255;
int pat_data_len=0;
int pat_count=0;
byte order[256];
unsigned long pat_offs[200];
unsigned long sam_offs;
unsigned long sam_raw_offs[100];
unsigned long inst_offs;
unsigned long msg_offs=0;

pat_row*new_pat_row(void) {
  int x;
  pat_row*row=malloc(sizeof(pat_row));
  if(!row) {
    fprintf(stderr,"*FATAL* Out of memory to make pattern rows.\n");
    exit(1);
  }
  for(x=0;x<18;x++) {
    row->note[x]=200;
    row->instrument[x]=row->volume[x]=row->effectid[x]=255;
  }
  row->next=0;
  return row;
}

inline void make_pattern_rows(void) {
  pat_row*row;
  int loop_start=0;
  int loop_skip=0;
  int loop_end=0;
  int x,y;
  byte chve[17]; // volume
  byte chxi[17]; // effect ID
  byte chxp[17]; // effect parameter
  int frameskip=0;
  pat_start=pat_loop_begin=pat_loop_skip=pat_loop_end=0;
  for(x=0;x<17;x++) chve[x]=chxi[x]=chxp[x]=255;
  for(x=0;x<16;x++) {
    if(channels[x].loopstart) {
      if(loop_start && channels[x].loopstart!=loop_start) fprintf(stderr,"Desynchronized loop start (%c).\n",x+'A');
      loop_start=channels[x].loopstart;
    }
    if(channels[x].loopskip) {
      if(loop_skip && channels[x].loopskip!=loop_skip) fprintf(stderr,"Desynchronized loop skip (%c).\n",x+'A');
      loop_skip=channels[x].loopskip;
    }
    if(channels[x].loopend) {
      if(loop_end && channels[x].loopend!=loop_end) fprintf(stderr,"Desynchronized loop end (%c).\n",x+'A');
      loop_end=channels[x].loopend;
    }
  }
  for(x=0;x<max_frames;x++) {
    if(frames[x].used || frameskip==15) {
      // use frame
      if(pat_start) {
        if(frameskip) {
          row->effectid[17]=FX('S');
          row->effectpar[17]=0xE0|frameskip;
        }
        row->next=new_pat_row();
        row=row->next;
      } else {
        row=pat_start=new_pat_row();
      }
      if(x==loop_start) pat_loop_begin=row;
      if(x==loop_skip) pat_loop_skip=row;
      if(x==loop_end) pat_loop_end=row;
      frameskip=0;
      for(y=0;y<17;y++) {
        chan_row*fr=frames[x].data+y;
        row->note[y]=200;
        row->volume[y]=chve[y];
        row->effectid[y]=chxi[y];
        row->effectpar[y]=chxp[y];
        if(fr->continue_flag&0x80) {
          row->note[y]=fr->note;
          row->instrument[y]=fr->instrument;
          row->volume[y]=fr->volumeset;
          row->effectid[y]=fr->effectid;
          row->effectpar[y]=fr->effectpar;
          if(fr->continue_flag&0x01) {
            chxi[y]=fr->effectid; chxp[y]=fr->effectpar;
          } else {
            chxi[y]=chxp[y]=255;
          }
          if(fr->continue_flag&0x02) chve[y]=fr->volumeset; else chve[y]=255;
          if(fr->continue_flag&0x04) chxp[y]=0;
        }
      }
    } else {
      // unuse frame
      frameskip++;
    }
  }
}

inline void make_pattern_data(void) {
  int rc; // row count per pattern
  pat_row*row=pat_start;
  byte*pat;
  byte*pp;
  byte lastnote[32];
  byte lastinstrument[32];
  byte lastvolumepan[32];
  byte lastcommand[32];
  byte lastcommandvalue[32];
  byte lastmask[32];
  byte mask;
  int x;
  boolean bs;

  pat_data=malloc(0);

next_pattern:
  bs=1;
  pat_offs[pat_count++]=pat_data_len;
  pat_data=realloc(pat_data,pat_data_len+=65536); // Note that the pattern + the 8 byte header will ALWAYS be less than 64k (if <47 channels)
  if(!pat_data) {
    fprintf("*FATAL* Out of pattern data memory for pattern %d.\n",pat_count);
    exit(1);
  }
  pat=pat_data+(pat_data_len-65536);
  pat[3]=pat[4]=pat[5]=pat[6]=pat[7]=0; // Len x x x x
  pp=pat+8;
  rc=0;
  for(x=0;x<32;x++) {
    lastnote[x]=200;
    lastinstrument[x]=lastvolumepan[x]=lastcommand[x]=lastcommandvalue[x]=lastmask[x]=255;
  }

next_row:
  if(rc==200) goto done_pattern;
  for(x=0;x<18;x++) {
    #define rowMask(xx,yy,zzz,zz) ((row->xx[x]!=yy)<<(zzz+4*(row->xx[x]!=zz[x])))
    #define do_rowMask rowMask(note,200,0,lastnote)|rowMask(instrument,255,1,lastinstrument)|rowMask(volume,255,2,lastvolumepan)|rowMask(effectid,255,3,lastcommand)
    mask=do_rowMask;
    if((mask&128) && row->effectpar[x]!=lastcommandvalue[x]) mask-=120;
    #undef rowMask
    #define rowMask(xx,yy,zzz,zz) (zz[x]=row->xx[x]!=yy?row->xx[x]:zz[x])
    do_rowMask;
    if(row->effectid[x]!=255) lastcommandvalue[x]=row->effectpar[x];
    #undef rowMask
    #undef do_rowMask
    *pp++=((mask==lastmask[x])<<7)|(x+1);
    if(mask!=lastmask[x]) *pp++=mask;
    if(mask&1) *pp++=row->note[x];
    if(mask&2) *pp++=row->instrument[x];
    if(mask&4) *pp++=row->volume[x];
    if(mask&8) *pp++=row->effectid[x],*pp++=row->effectpar[x];
    lastmask[x]=mask;
  }
  if(row==pat_loop_begin || row==pat_loop_skip || row==pat_loop_end) {
    *pp++=128|19;
    *pp++=FX('B');
    if(row==pat_loop_begin) { where_loopstart_ord=pp-pat_data; pattern_loopstart=pat_count-1; }
    if(row==pat_loop_skip) { where_loopskip_ord=pp-pat_data; pattern_loopskip=pat_count-1; }
    if(row==pat_loop_end) { where_loopend_ord=pp-pat_data; pattern_loopend=pat_count-1; }
    *pp++=0xFF; // placeholder
    bs=0;
  }
  *pp++=0;
  rc++;
  if(row=row->next) if(bs) goto next_row;

done_pattern:
  if(bs && rc<32) {
    pp[-1]=128|19; // Channel 18 (override last terminator)
    *pp++=8; // use effect
    *pp++=FX('C'); // break to row
    *pp++=0x00; // row 0
    *pp++=0; // terminator
  }
  while(rc<32) rc++,*pp++=0;
  pat[2]=rc;
  pat[1]=((pp-pat)-8)>>8;
  pat[0]=((pp-pat)-8)&0xFF;
  pat_data=realloc(pat_data,pat_data_len+=(pp-pat)-65536);
  if(row) goto next_pattern;
}

inline void make_orders(void) {
  int o,p;

  // Reset orders
  for(o=0;o<256;o++) order[o]=255;
  o=p=0;

  if(pattern_loopstart==255) {
    // Make simple order
    while(p<pat_count) order[o++]=p++;
  } else {
    // Make order before loop start
    p=0;
    while(p<=pattern_loopstart) order[o++]=p++;
    pat_data[where_loopend_ord]=o;

    if(pattern_loopskip!=255) {
      // Make order between loop skip and loop end (order is switch between skip..end/start..skip)
      p=pattern_loopskip+1;
      while(p<=pattern_loopend) order[o++]=p++;
      pat_data[where_loopskip_ord]=o;

      // Make order between loop start and loop skip
      p=pattern_loopstart+1;
      while(p<=pattern_loopskip) order[o++]=p++;
      pat_data[where_loopstart_ord]=o;
    }

    // Make order after loop end
    p=pattern_loopend+1;
    while(p<pat_count) order[o++]=p++;
  }
}

inline void output_calc(void) {
  make_pattern_rows();
  make_pattern_data();
  if(pat_count>200) fprintf(stderr,"Too many patterns.\n");
  make_orders();
  //todo
}

inline void output_main(void) {
  fwrite("IMPM",1,4,stdout);                // File type identification
  fwrite(song_title,1,26,stdout);           // Song Name
  write16(0);                               // PHiligt
  //todo
}

//=========================================//

int main(int argc,char**argv) {
  argv+=!!argc; argc-=!!argc;
  while(argc--) {
    if(**argv=='-') {
      char*o=*argv+1;
      char*s;
      while(*o) {
        switch(*o) {
          case 'c': // Compatibility setting
            s=*++argv; argc--;
            compatibility=read_int(&s);
            break;
          case 'e': // Export sample to stdout
            s=*++argv; argc--;
            export_sample=read_int(&s);
            break;
          case 'p': // Prefix for filename when loading external samples
            filename_prefix=*++argv; argc--;
            break;
          case 't': // Trace input
            trace_input=1;
            break;
          case 'v': // Version
            puts(itmck_version);
            return 0;
          case 'w': // Set Cwt header
            s=*++argv; argc--;
            tracker_version=read_int(&s);
            break;
          case 'x': // Volume 0 mix optimization
            it_flags|=0x02;
            break;
          default:
            fprintf(stderr,"*FATAL* Unrecognized switch: -%c\n",*o);
            return 1;
        }
        o++;
      }
    } else if(**argv) {
      fprintf(stderr,"*FATAL* Unrecognized command-line option: %s\n",*argv);
      return 1;
    }
    argv+=!!argc;
  }

  make_defaults();
  read_file(stdin);

#ifdef _WIN32
  _setmode(_fileno(stdout),_O_BINARY);
#endif

  // Export sample
  if(export_sample) {
    do_export_sample();
    return 0;
  }

  // Output main
  output_calc();
  output_main();
  return 0;
}

// http://16-bits.org/it/ for information of .IT format
// http://woolyss.com/chipmusic/chipmusic-mml/ppmck_guide.php for MCK
